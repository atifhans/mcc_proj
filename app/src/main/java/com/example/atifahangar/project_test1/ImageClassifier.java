package com.example.atifahangar.project_test1;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;

import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class ImageClassifier {

    //private static final String MODEL_PATH = "mobilenet_quant_v1_224.tflite";
    //private static final String LABEL_PATH = "labels.txt";
    private static final String MODEL_PATH = "inceptionv3_slim_2016.tflite";
    private static final String LABEL_PATH = "imagenet_slim_labels.txt";
    private static final int RESULTS_TO_SHOW = 3;
    private static final int BATCH_SIZE = 1;
    private static final int PIXEL_SIZE = 3;

    //static final int IMG_SIZE_X = 224;
    //static final int IMG_SIZE_Y = 224;
    static final int IMG_SIZE_X = 299;
    static final int IMG_SIZE_Y = 299;
    private static final int IMAGE_MEAN = 128;
    private static final float IMAGE_STD = 128.0f;

    private int[] intValues = new int[IMG_SIZE_X * IMG_SIZE_Y];
    private Interpreter tflite;
    private List<String> labelList;
    private ByteBuffer imgData;
    //private byte[][] labelProbArray;
    private float[][] labelProbArray;

    private PriorityQueue<Map.Entry<String, Float>> sortedLabels =
            new PriorityQueue<>(RESULTS_TO_SHOW, (o1, o2) -> (o1.getValue()).compareTo(o2.getValue()));

    ImageClassifier(Activity activity) throws IOException {
        tflite = new Interpreter(loadModelFile(activity));
        labelList = loadLabelList(activity);
        //imgData = ByteBuffer.allocateDirect(BATCH_SIZE * IMG_SIZE_X * IMG_SIZE_Y * PIXEL_SIZE);
        imgData = ByteBuffer.allocateDirect(BATCH_SIZE * IMG_SIZE_X * IMG_SIZE_Y * PIXEL_SIZE * 4);
        imgData.order(ByteOrder.nativeOrder());
        //labelProbArray = new byte[1][labelList.size()];
        labelProbArray = new float[1][labelList.size()];
    }

    String classifyFrame(Bitmap bitmap) {
        if (tflite == null) {
            return "Uninitialized Classifier.";
        }
        convertBitmapToByteBuffer(bitmap);

        tflite.run(imgData, labelProbArray);

        return printTopLabel();
    }

    public void close() {
        tflite.close();
        tflite = null;
    }

    private MappedByteBuffer loadModelFile(Activity activity) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_PATH);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    private List<String> loadLabelList(Activity activity) throws IOException {
        List<String> labelList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(activity.getAssets().open(LABEL_PATH)));
        String line;
        while ((line = reader.readLine()) != null) {
            labelList.add(line);
        }
        reader.close();
        return labelList;
    }

    private void convertBitmapToByteBuffer(Bitmap bitmap) {
        if (imgData == null) {
            return;
        }
        imgData.rewind();
        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        int pixel = 0;
        for (int i = 0; i < IMG_SIZE_X; ++i) {
            for (int j = 0; j < IMG_SIZE_Y; ++j) {
                final int val = intValues[pixel++];
                //imgData.put((byte) ((val >> 16) & 0xFF));
                //imgData.put((byte) ((val >> 8) & 0xFF));
                //imgData.put((byte) (val & 0xFF));
                imgData.putFloat((((val >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat((((val >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat(((val & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
            }
        }
    }

    private String printTopLabel() {
        for (int i = 0; i < labelList.size(); ++i) {
            sortedLabels.add(
                    //new AbstractMap.SimpleEntry<>(labelList.get(i), (labelProbArray[0][i] & 0xff) / 255.0f));
                new AbstractMap.SimpleEntry<>(labelList.get(i), labelProbArray[0][i]));
            if (sortedLabels.size() > RESULTS_TO_SHOW) {
                sortedLabels.poll();
            }
        }
        String textToShow = "";

        Map.Entry<String, Float> label = sortedLabels.poll();
        if(label.getValue() > 0.3) {
            textToShow = String.format("%s", label.getKey()) + textToShow;
        }
        else {
            textToShow = "Detecting";
        }

        return textToShow;
    }
}
